package model;

import java.sql.SQLException;

public class Comment extends DBConnect{
	
	/*
	 * ------------------------------------------------------------------------------------
	 * INSERT INTO COMMENTS TABLE QUERIES
	 * ------------------------------------------------------------------------------------
	 */
	public void addComment(int blogId, int userId, String comment){
		String sql = "INSERT INTO \"comments\" (\"user_id\", \"blog_id\", \"comments\", \"created_at\") "
				+ "VALUES (?,?,?, SYSDATE)";
		
		try {
			stmt = con.prepareStatement(sql);
			stmt.setInt(1, userId);
			stmt.setInt(2, blogId);
			stmt.setString(3, comment);
			
			int flag = stmt.executeUpdate();
			
			if (flag == 1){
				System.out.println("Comment has been added to the blog.");
			}else{
				System.out.println("There was some error saving your comment!");
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} catch(Exception e){
			e.printStackTrace();
		}
	}
}
